import logging
import src.utils as utils
import src.containers as containers
import matplotlib.pyplot as plt


if __name__ == "__main__":

    # Setup all loggers
    utils.setupLoggers()
    logger = logging.getLogger(__name__)

    logger.info("Welcome to the clock stability analyser! \U0001F680")
    logger.info("commit SHA:\t%s\n", utils.get_git_revision_hash())

    # load the config file with all experimental parameters
    logger.info("Loading configuration parameters ...")
    yaml_params = utils.load_yaml_config()
    clock1_kwargs = yaml_params.clock1
    clock2_kwargs = yaml_params.clock2
    raw = yaml_params.raw
    plot = yaml_params.plot
    show = yaml_params.show
    logger.info("\tDONE\n")

    # MAIN

    logger.info("Setting up Clock #1 ...")
    sr1 = containers.Clock(**clock1_kwargs)
    logger.info("\tDONE")
    logger.info("\n%s", sr1)

    logger.info("Setting up Clock #2 ...")
    sr2 = containers.Clock(**clock2_kwargs)
    logger.info("\tDONE")
    logger.info("\n%s", sr2)

    logger.info("Synchronising Clocks ...")
    sr1sr2 = sr1 + sr2
    logger.info("\tDONE")
    logger.info("\n%s", sr1sr2)

    # Plot
    if plot:
        logger.info("Plotting...")

        try:
            sr1sr2.clock1.render_all_servos()
            sr1sr2.clock2.render_all_servos()
            sr1sr2.clock1.render_bbr()
            sr1sr2.clock2.render_bbr()
            sr1sr2.clock1.render_stats()
            sr1sr2.clock2.render_stats()
            sr1sr2.render_correlations()
            sr1sr2.render_servo_corrections()
            sr1sr2.render_plot3_diff()
            sr1sr2.render_instability(raw=raw)

        finally:  # plot up to the first one that failed
            if show:
                plt.show()
            else:
                pass

    logger.info("All done!")

