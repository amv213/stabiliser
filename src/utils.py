import yaml
import logging
import logging.config
import datetime
import subprocess
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
from typing import Tuple, List, Dict, TypedDict, Union, NamedTuple, overload

# Spawn module-level logger
logger = logging.getLogger(__name__)


# Type all Yaml Config parameters and build NamedTuple holding all of them
class ClockParams(TypedDict):
    root: str
    label: str
    transition: Tuple[float]
    spec_time: float
    spec_type: str
    contrast: float
    servo_seq: List[int]
    centre_frequency: float
    atom_scaling: float
    static_correction: float
    max_gap: float
    threshold_date_low: float
    threshold_date_high: float
    build_on_creation: bool


YamlParams = NamedTuple('YamlParams', [('clock1', ClockParams),
                                       ('clock2', ClockParams),
                                       ('raw', bool),
                                       ('plot', bool),
                                       ('show', bool),
                                       ('channels', Dict[str, List[str]])
                                       ])


def load_yaml_config(fn: Union[str, Path] = 'config.yaml') -> YamlParams:
    """Loads experimental parameters from YAML configuration file.

    Example:

        .. code-block:: python

            yaml_params = utils.load_yaml_config()

            clock1 = yaml_params.clock1

    Args:
        fn (optional):  path to YAML configuration file. Defaults to
                        ``config.yaml``.

    Returns:
        NamedTuple of experimental parameters loaded from YAML configuration
        file. Individual fields are accessed through the key they are
        referenced by in the YAML file.
    """

    # By default, configuration file should be in ../references/
    conf_dir = Path(__file__).parent.parent / 'references'
    fn = conf_dir / fn
    logger.debug("\t[%s]", Path(fn).resolve())

    try:

        # Load from YAML configuration file
        with open(fn, 'rb') as f:
            conf = yaml.load(f, Loader=yaml.FullLoader)
            # ^ dict with a key for each top-level key in the YAML file

        # Filter out yaml entries which have not been included in NamedTuple
        # definition (this is to avoid pulling in global shared entries)
        conf = {k: conf[k] for k in YamlParams._fields}

        # Unpack all yaml elements in corresponding NamedTuple fields
        return YamlParams(**conf)

    except FileNotFoundError as e:
        logger.warning('Could not locate the %s configuration file', fn)
        raise


def setupLoggers(fn: Union[str, Path] = 'logging.yaml') -> None:
    """Reads in logging configuration file and sets up all loggers as
    prescribed.
    """

    # Setup the loggers from configuration file (stderr + disk, DEBUG)
    conf_dir = Path(__file__).parent.parent / 'references'
    with open(conf_dir / fn, 'r') as f:
        log_config = yaml.safe_load(f.read())

        # Create the output logs directory if doesn't exist
        logs_dir = Path(log_config['handlers']['file']['filename']).parent
        if not logs_dir.is_dir():
            logs_dir.mkdir(parents=True, exist_ok=True)

        logging.config.dictConfig(log_config)


def get_git_revision_hash(short: bool = True) -> str:
    """Returns the git commit SHA for the code being run.

    Args:
        short (optional):   if ``True`` truncates the commit SHA to the
                            first 8 characters only. Defaults to ``True``.

    Returns:
        git commit SHA.

    References:
        https://stackoverflow.com/a/21901260
    """

    sha = subprocess.check_output(['git', 'rev-parse', 'HEAD'])
    sha = sha.decode('ascii').strip()

    return sha[:8] if short else sha


@overload
def labview_timestamp_to_unix(timestamp: float) -> float: ...


@overload
def labview_timestamp_to_unix(timestamp: np.ndarray) -> np.ndarray: ...


def labview_timestamp_to_unix(timestamp: Union[float, np.ndarray, pd.Series]
                              ) -> Union[float, np.ndarray]:
    """Converts LabView timestamps (number of seconds elapsed since epoch) to
    their corresponding UNIX timestamps.

    Args:
        timestamp: one or more LabView timestamps, in seconds.

    Returns:
        corresponding UNIX timestamp(s), in seconds.

    """
    # LabView epoch starts in 1904 (66 years before UNIX epoch)
    labview_epoch = datetime.datetime(1904, 1, 1, 0, 0)
    unix_epoch = datetime.datetime(1970, 1, 1, 0, 0)

    offset = (unix_epoch - labview_epoch).total_seconds()

    return timestamp - offset


def sanitise_timestamps(df: pd.DataFrame) -> pd.DataFrame:
    """Parses a dataframe of timestamped metrics nominally collected at a
    regular rate. Missing entries are filled with synthesised surrogates.
    Entries with duplicate timestamps are updated with expected values.

    Args:
        df: data to be sanitised. Must contain a `Time Stamp` field.

    Returns:
        sanitised data.
    """

    # Median time interval between entries, rounded to us
    # Some entries might be missing, or be duplicate so need robust
    # estimator of rate
    avg_shot_sep = df['Time Stamp'].diff().median()
    logger.debug("\t\t\t\t\tmedian servo data sampling rate: %.6f s",
                 avg_shot_sep)

    logger.debug("\t\t\t\t\tregularising on uniform grid")

    # Create an homogeneous grid
    df['Time Stamp'] = pd.to_datetime(df['Time Stamp'], unit='s')
    grid = pd.date_range(start=df['Time Stamp'].iloc[0],
                         end=df['Time Stamp'].iloc[-1],
                         freq=f'{avg_shot_sep*10**6:.0f}us'
                         ).to_frame(name='Time Stamp')
    # ^ Used to have freq=f'{avg_shot_sep*10**3:.6f}ms'

    # Transfer data over to the grid
    df = pd.merge_asof(grid, df, left_on='Time Stamp',
                       right_on='Time Stamp', direction='nearest',
                       tolerance=pd.Timedelta(f"{avg_shot_sep/2}s"))

    # Convert back datetimes to unix timestamps
    df['Time Stamp'] = (df['Time Stamp'] - pd.Timestamp("1970-01-01")) / pd.Timedelta('1s')

    return df


def sync_clock(df: pd.DataFrame, pattern: list) -> pd.DataFrame:
    """Trims a dataframe to start and end in sync with the first and last
    observation of the given servo pattern.

    Args:
        df:         the dataframe to be trimmed. Must contain a `Servo` field.
        pattern:    the servo pattern to match against.

    Returns:
        dataframe trimmed to be synchronised with the required servo pattern.
    """

    # Start on row where servo sequence kicking in
    idx_start_seq = find_start_seq(df, pattern=pattern)
    df = df.iloc[idx_start_seq:]

    # End on last complete servo sequence iteration
    idx_stop_seq = find_stop_seq(df, pattern=pattern)
    df = df.iloc[:idx_stop_seq + 1]

    # This algorithm doesn't check that the servo pattern is repeated
    #  throughout between start and end indexes. This could happen if
    #  timestamps slightly dephase... so assert it is the case here
    # Make sure servo sequence is monotonic from start to finish
    num_shots = len(df) // len(pattern)
    with pd.option_context('display.float_format', '{:0.20f}'.format):
        assert np.array_equiv(df['Servo'].values, pattern*num_shots), \
            f"Servo data is not monotonic\n{df['Servo'].values}"

    # Re-build index column with unique indexes
    df = df.reset_index(drop=True)

    return df


def find_start_seq(df: pd.DataFrame, pattern: list) -> int:
    """Return the starting index at which the given servo pattern is first
    observed in the dataframe.

    Args:
        df:         the dataframe to be trimmed. Must contain a `Servo` field.
        pattern:    the servo pattern to match against.

    Returns:
        the index at which the pattern starts.
    """

    # Flag candidate starting points
    first_servo = pattern[0]
    idxs_first_servo_starts = np.flatnonzero(df['Servo'] == first_servo)

    # Loop through possible starting points and check if the observed chunk
    # matches the servo pattern sought after
    idx_start_synch = 0
    for idx in idxs_first_servo_starts:
        servo_sequence = df.iloc[idx:idx+len(pattern)]['Servo']
        if np.array_equal(servo_sequence.values, pattern):
            idx_start_synch = idx
            break

    return idx_start_synch


def find_stop_seq(df: pd.DataFrame, pattern: list) -> int:
    """Return the last index at which the given servo pattern is last
    observed in the dataframe.

    Args:
        df:         the dataframe to be trimmed. Must contain a `Servo` field.
        pattern:    the servo pattern to match against.

    Returns:
        the last index at which the pattern is last observed.
    """

    # Flag candidate ending points
    last_servo = pattern[-1]
    idx_good_servo_ends = np.flatnonzero(df['Servo'] == last_servo)

    # Loop through possible ending points and check if the observed chunk
    # preceding it matches the servo pattern sought after
    idx_stop_synch = 0
    for idx in idx_good_servo_ends[::-1]:  # loop from bottom
        servo_sequence = df.iloc[idx+1-len(pattern):idx+1]['Servo']
        if np.array_equal(servo_sequence.values, pattern):
            idx_stop_synch = idx
            break

    return idx_stop_synch


def calc_plot1(ef, centre_freq):
    """

    Args:
        ef: panda series of Excitation Fractions in servo order 1, 3, 2, 4
        centre_freq:

    Returns:

    """

    ef = ef.values
    servo1_ef = ef[0]
    servo2_ef = ef[1]

    return 0.5 * (servo1_ef + servo2_ef) - centre_freq


def calc_plot2(ef, centre_freq):

    # If there are servos 3 and 4
    try:
        ef = ef.values
        servo3_ef = ef[2]
        servo4_ef = ef[3]

        return 0.5 * (servo3_ef + servo4_ef) - centre_freq
    # if not, plot 2 cannot be calculated
    except IndexError:
        return np.NaN


def calc_sss1(ef):

    ef = ef.values
    servo1_ef = ef[0]
    servo2_ef = ef[1]

    return servo1_ef - servo2_ef


def calc_sss2(ef):

    # If there are servos 3 and 4
    try:
        ef = ef.values
        servo3_ef = ef[2]
        servo4_ef = ef[3]

        return servo3_ef - servo4_ef
    # if not, sss2 cannot be calculated
    except IndexError:
        return np.NaN


def calc_2o_zeeman1(ef, coefficient):

    return coefficient * np.abs(calc_sss1(ef))**2


def calc_2o_zeeman2(ef, coefficient):

    return coefficient * np.abs(calc_sss2(ef))**2


def flag_outliers(series: pd.Series, m: float = 3.5,
                  discard_below: bool = False,
                  threshold: float = 3) -> pd.Series:
    # https://stackoverflow.com/questions/11686720/is-there-a-numpy-builtin-to-reject-outliers-from-a-list

    # discard all points below a certain value when calculating median
    if discard_below:
        series = series.where(series.ge(series.max()/threshold))

    # Calculate median
    med = series.median()
    logger.debug("\t\t\t\t\tmedian for outliers detection set at %.7e", med)

    # Calculate MAD
    d = series - med
    ad = d.abs()
    mad = ad.median()

    # Calculate modified Z-score
    s = 0.6745*d/mad
    s = s.abs()

    # isna required to make mask evaluate to True (i.e outliers)
    outlier_flag = (s > m) | s.isna()

    logger.debug("\t\t\t\t\tflagged %i outliers / %i points",
                 outlier_flag.sum(), len(outlier_flag))
    return outlier_flag


def flag_below_threshold(series: pd.Series, threshold: float = 3) -> pd.Series:
    """Simple function to just flag values below certain threshold. Without
    then removing outliers from remaining data.

    Args:
        series:
        threshold:

    Returns:

    """

    logger.debug("\t\t\t\t\tthreshold for outliers detection set at %.2f",
                 series.max()/threshold)

    # flag all points below a certain value
    outlier_flag = series.le(series.max()/threshold)

    logger.debug("\t\t\t\t\tflagged %i outliers / %i points",
                 outlier_flag.sum(), len(outlier_flag))
    return outlier_flag


def estimate_qpn_instability(srs: pd.DataFrame, tc, tp, c, spec_type, f0
                             ) -> float:
    """Calculate average expected qpn instability for given atom numbers"""

    linewidth = 0.8 / tp if spec_type == 'Rabi' else 0.5 / tp
    q = f0/linewidth
    n = srs.mean()

    sql = 1/c * 1 / (np.pi * q) * np.sqrt(tc/n)

    return sql


def build_diff_df(df1, df2, metric):
    # Create dataframe for inter-clock comparison statistics. Nominal
    # time stamp of each statistic value is set at the average value of
    # the time stamps that produce it

    # Initialise new df
    df_diff = pd.DataFrame()

    # Set timestamps to virtual mean of timestamps were individual
    # statistics were taken by each clock
    df_diff['Time Stamp'] = 0.5 * (df1['Time Stamp'] + df2['Time Stamp'])

    # Calculate difference
    df_diff['Plot Diff'] = df2[metric] - df1[metric]

    # Add extra columns for aligned individual metrics
    df_diff[f'{metric} C1'] = df1[metric]
    df_diff[f'{metric} C2'] = df2[metric]

    # Collapse Validity Flags to new dimensions
    df_diff['Flag Run Valid'] = df1['Flag Run Valid'] & df2['Flag Run Valid']
    df_diff['Flag Run Selected'] = df1['Flag Run Selected'] & \
                                   df2['Flag Run Selected']

    return df_diff


def virtualize_stats(df1: pd.DataFrame, df2: pd.DataFrame) -> pd.DataFrame:
    """Merges two statistics dataframes into a single dataframe on a
    virtualized synchronous timestamp grid.

    Args:
        df1:
        df2:

    Returns:

    """

    if not np.array_equiv(df1.columns, df2.columns):
        raise ValueError("Cannot virtualize the two input dataframes. "
                         "Dataframes don't have the same statistics.")

    # Initialise new df
    df = pd.DataFrame()

    # Set timestamps to virtual mean of timestamps were individual
    # statistics were taken by each clock
    df['Time Stamp'] = 0.5 * (df1['Time Stamp'] + df2['Time Stamp'])

    # Add extra columns for aligned individual metrics
    metrics = [m for m in df1.columns if not (m.startswith('Flag') or
                                              m.startswith('Time Stamp'))]
    for metric in metrics:
        df[f'{metric} C1'] = df1[metric]
        df[f'{metric} C2'] = df2[metric]

    # Collapse Validity Flags to new dimensions
    df['Flag Run Valid'] = df1['Flag Run Valid'] & df2['Flag Run Valid']
    df['Flag Run Selected'] = df1['Flag Run Selected'] & \
                              df2['Flag Run Selected']

    return df


def save_fig(label: str) -> None:
    """Utility function saving latest plot object to disk.

    Figure is saved in a ``../reports/figures/`` directory

    Args:
        label: name under which to save the image.
    """

    save_dir = Path(__file__).parent.parent / 'reports' / 'figures'
    if not save_dir.is_dir():
        save_dir.mkdir(parents=True, exist_ok=True)

    fn = save_dir / label
    logger.debug("\t\tsaving figure [%s]", fn)

    plt.savefig(fn, dpi=300, transparent=False)


def save_data(df: pd.DataFrame, metric: str, fn: str) -> None:
    """Utility function saving timestamped data from dataframe.

    Figure is saved in a ``../data/processed/`` directory

    Args:
        df:     dataframe holding data to be saved.
        metric: column name of the data to be saved.
        fn:     name under which to save the data.

    """

    save_dir = Path(__file__).parent.parent / 'data' / 'processed'
    if not save_dir.is_dir():
        save_dir.mkdir(parents=True, exist_ok=True)

    fn = save_dir / fn
    logger.debug("\t\tsaving fractional frequency data [%s]", fn)

    # Write header metadata
    with open(fn, 'w') as f:
        f.write(f"# Date: {datetime.datetime.now()}\n")
        f.write(f"# Points: {df[metric].size}\n")
        f.write(f"# Tau:\t{df['Time Stamp'].diff().mean():.6e}\n")
        f.write('# Header End\n')

    # Write data
    df.to_csv(fn, sep='\t', columns=['Time Stamp', metric], index=False,
              header=False, mode='a')


def explode(x: pd.Series, n: int) -> np.ndarray:
    """Explodes input series by repeating each element a given number of times.

    Args:
        x:  input series.
        n:  size by which to explode each element.

    Returns:
        input data where each row element has been repeated ``n`` times.
    """

    xploded = x.loc[x.index.repeat(n)].reset_index(drop=True)

    return xploded.values


