import logging
import allantools
import src.utils as utils
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.gridspec import GridSpec
from datetime import datetime
from scipy import signal
from matplotlib.patches import Ellipse

# Spawn module-level logger
logger = logging.getLogger(__name__)


def plot_timeseries(df: pd.DataFrame, metric: str, label: str,
                    median: bool = True, ax: plt.Axes = None,
                    is_flagged: bool= True) -> plt.Axes:

    if ax is None:
        fig, ax = plt.figure()

    # Local copy to avoid memory leakage
    df = df.copy()

    # Convert timestamps to human-readable datetimes
    df['Time Stamp'] = df['Time Stamp'].apply(datetime.fromtimestamp)

    # Split into valid and invalid data
    if is_flagged:
        dfx = df[metric].where(df['Flag Run Selected'] == False)
        dfv = df[metric].where(df['Flag Run Selected'] == True)
    else:
        dfv = df[metric]
        dfx = pd.Series(False, index=dfv.index)
        
    if median:
        if not dfv.isnull().all():
            med = dfv.median()
            std = dfv.std()
            ax.axhline(med, ls='--', c='LightGray')

            if std != 0:
                ax.set_ylim(med - 3*std, med + 3*std)

            logger.debug("\t\t\t\ttimeseries median = %.7e", med)
            logger.debug("\t\t\t\ttimeseries 1-sigma half-width = %.7e", std)

    ax.plot(df['Time Stamp'], dfx, 'x', c='DarkGray')
    ax.plot(df['Time Stamp'], dfv, c='Black')

    ax.set_ylabel(f'{label}')

    if not df.empty:
        ax.set_xlim(df['Time Stamp'].min(), df['Time Stamp'].max())

    return ax


def plot_timeseries_hist(srs, color, orientation='vertical', ax=None):

    if ax is None:
        fig, ax = plt.figure()

    if not srs.isnull().values.all():

        # Histogram
        n, bins, patches = ax.hist(srs, bins=int(np.sqrt(srs.count())),
                                   density=True, orientation=orientation,
                                   rwidth=0.8, color=color)

        sigma = srs.std()
        mu = srs.mean()
        med = srs.median()
        logger.debug("\t\t\t\thistogram mean = %.7e", mu)
        logger.debug("\t\t\t\thistogram median = %.7e", med)
        logger.debug("\t\t\t\thistogram 1-sigma half-width = %.7e", sigma)

        # Create a Gaussian kernel based on mean and median
        ax = plot_gaussian_kernel(mu=mu, sigma=sigma, x=bins,
                                  orientation=orientation,
                                  c='Black', ls='-', ax=ax)
        ax = plot_gaussian_kernel(mu=med, sigma=sigma, x=bins,
                                  orientation=orientation,
                                  c='LightGray', ls='--', ax=ax)

    return ax


def plot_gaussian_kernel(mu: float, sigma: float, x: np.ndarray,
                         orientation: str,
                         c: str, ls: str, ax: plt.Axes) -> plt.Axes:

    y = ((1 / (np.sqrt(2 * np.pi) * sigma)) *
         np.exp(-0.5 * (1 / sigma * (x - mu)) ** 2))

    # If plotting a sideways histogram, swap x, y
    if orientation == 'horizontal':
        y, x = x, y

    ax.plot(x, y, c=c, ls=ls)

    return ax


def plot_timeseries_adev(srs, tau, label, color, ax=None):

    if ax is None:
        fig, ax = plt.figure()

    if not srs.isnull().values.all():

        t2, adevs, errors, ns = allantools.totdev(srs.values, data_type='freq')
        errors = errors * np.sqrt(t2) / np.sqrt(2)
        t2 *= tau
        ax.errorbar(t2, adevs, errors, fmt='-o', label=label, color=color)

    ax.set_xscale("log", nonpositive='clip')
    ax.set_yscale("log", nonpositive='clip')
    ax.grid(True, which='major', alpha=0.5)
    ax.grid(True, which='minor', alpha=0.3)
    ax.set_ylabel('Total Allan Deviation')
    ax.set_xlabel('Averaging Time (s)')
    ax.set_ylim(1e-19, 1e-15)

    return ax


def plot_plot3_stats(df):

    df = df.copy()  # make copy to avoid leakage of changes

    # Collapse dataframe to only valid statistics
    df = df.loc[df['Flag Stat Valid'] == True]

    # Average cycle time (average of average cycle time in Sr1 and Sr2)
    tau = 0.5 * (df.loc[0]['Time Stamp'].diff().mean()
                 + df.loc[1]['Time Stamp'].diff().mean())
    print(f"Sequence cycle time: {tau} s")

    # Calculate plot 3 metrics and flag outliers
    plot3_c0 = df.loc[0]['Plot 3']
    plot3_c1 = df.loc[1]['Plot 3']
    plot3_difference = plot3_c1 - plot3_c0
    outliers_flag = utils.flag_outliers(plot3_difference, m=5)
    plot3_difference = plot3_difference.loc[~outliers_flag]

    # Calculate estimated QPN instability:
    qpn_c0 = df.loc[0]['Estimated QPN'].mean()
    qpn_c1 = df.loc[1]['Estimated QPN'].mean()
    qpn = np.sqrt(qpn_c0**2 + qpn_c1**2)

    fig = plt.figure(figsize=(15, 9))

    fig.suptitle(f"Plot3 Dashboard")

    # Blueprint, setup subplots layout
    gs1 = GridSpec(3, 3)  # num_rows, num_columns

    ax1 = fig.add_subplot(gs1[0, 0:2])
    ax2 = fig.add_subplot(gs1[0, 2], sharey=ax1)
    ax3 = fig.add_subplot(gs1[1:3, 0:3])

    if not plot3_difference.isnull().values.all():
        ax1 = plot_timeseries(plot3_difference, tau=tau, ax=ax1)
        ax2 = plot_timeseries_hist(plot3_difference, color='DarkGray',
                                   orientation='horizontal', ax=ax2)
        ax3 = plot_timeseries_adev(plot3_c0, tau=tau, label='Plot 3 Sr1',
                                   color='LightGray', ax=ax3)
        ax3 = plot_timeseries_adev(plot3_c1, tau=tau, label='Plot 3 Sr2',
                                   color='DarkGray', ax=ax3)
        ax3 = plot_timeseries_adev(plot3_difference, tau=tau,
                                   label='Difference', color='Black', ax=ax3)
        taus = tau * np.arange(1, plot3_difference.count() + 1)
        ax3.plot(taus, np.sqrt(2)*qpn/np.sqrt(taus),
                 '--', color='Black', label='SQL')



    ax3.legend()
    ax3.set_xscale("log", nonpositive='clip')
    ax3.set_yscale("log", nonpositive='clip')
    ax3.grid(True, which='major')
    ax3.grid(True, which='minor', alpha=0.3)
    ax3.set_ylabel('Total Allan Deviation')
    ax3.set_xlabel('Averaging Time (s)')

    return ax1, ax2, ax3


def plot_interleaved_stats(df):

    df = df.copy()  # make copy to avoid leakage of changes

    # Collapse dataframe to only valid statistics
    df = df.loc[df['Flag Stat Valid'] == True]

    # FIXME: this is being repeatedly calculated across several
    #  clock_stat_data functions. Also should switch to calculate `Cycle Time`
    #  in the stats dataframe for each clock and just taking the average
    # Average clock cycle time
    tau = 0.5 * (df.loc[0]['Time Stamp'].diff().mean()
                 + df.loc[1]['Time Stamp'].diff().mean())

    # Calculate interleaved clock comparison
    clk_diff = df.loc[1]['Plot 1'] - df.loc[0]['Plot 1']
    clk_diff += df.loc[1]['Static Correction'] - df.loc[0]['Static Correction']

    # If there is a plot 2 (i.e servos 3 and 4)
    if not df['Plot 2'].isnull().values.all():
        # Interleave
        comp_b = df.loc[1]['Plot 2'] - df.loc[0]['Plot 2']
        comp_b += df.loc[1]['Static Correction'] - df.loc[0]['Static Correction']
        clk_diff = pd.concat([clk_diff, comp_b]).sort_index()
        # Reset the indexing of `Runs`. We have now synthesised 2x more.
        clk_diff = clk_diff.reset_index(drop=True)

        # Interleaving servo pairs gives 2x sped up
        tau /= 2

    # Clean data
    outliers_flag = utils.flag_outliers(clk_diff, m=5)
    interleaved = clk_diff.loc[~outliers_flag]
    if not interleaved.isnull().values.all():
        # FIXME: Check if should detrend before removing outliers
        interleaved_dedrifted = pd.Series(signal.detrend(interleaved))

    print(f"Interleaved Sequence cycle time: {tau} s")

    # Calculate estimated QPN instability:
    qpn_c0 = df.loc[0]['Estimated QPN'].mean()
    qpn_c1 = df.loc[1]['Estimated QPN'].mean()
    qpn = np.sqrt(qpn_c0**2 + qpn_c1**2)

    fig = plt.figure(figsize=(15, 9))

    fig.suptitle(f"Interleaved Statistics Dashboard")

    # Blueprint, setup subplots layout
    gs1 = GridSpec(3, 3)  # num_rows, num_columns

    ax1 = fig.add_subplot(gs1[0, 0:2])
    ax2 = fig.add_subplot(gs1[0, 2], sharey=ax1)
    ax3 = fig.add_subplot(gs1[1:3, 0:3])

    if not interleaved.isnull().values.all():
        ax1 = plot_timeseries(interleaved, tau=tau, ax=ax1)
        ax2 = plot_timeseries_hist(interleaved, color='DarkGray',
                                   orientation='horizontal', ax=ax2)
        ax3 = plot_timeseries_adev(interleaved, tau=tau,
                                   label='Interleaved Comparison',
                                   color='DarkGray', ax=ax3)
        ax3 = plot_timeseries_adev(interleaved_dedrifted, tau=tau,
                                   label='Linear Drift Removed', color='Black',
                                   ax=ax3)

        taus = tau * np.arange(1, interleaved.count() + 1)
        ax3.plot(taus, qpn/np.sqrt(taus),
                 '--', color='Black', label='SQL')

    ax3.legend()
    
    ax3.set_xscale("log", nonpositive='clip')
    ax3.set_yscale("log", nonpositive='clip')
    ax3.grid(True, which='major')
    ax3.grid(True, which='minor', alpha=0.3)
    ax3.set_ylabel('Total Allan Deviation')
    ax3.set_xlabel('Averaging Time (s)')

    return ax1, ax2, ax3


def plot_ci(x: pd.Series, y: pd.Series, sigmas: int, ax: plt.Axes = None) -> \
        plt.Axes:
    """Plot confidence interval ellipses for a given bivariate distribution.

    Args:
        x:
        y:
        sigmas:
        ax:

    Returns:

    """

    if ax is None:
        fig, ax = plt.figure()

    # Calculate correlations only if there is non NaN data to plot
    if not x.isnull().values.all() and not y.isnull().values.all():

        cov = np.cov(x, y)
        eigenval, eigenvec = np.linalg.eig(cov)
        eigenval = np.sqrt(eigenval)

        for j in range(1, sigmas+1):
            ell = Ellipse(xy=(x.mean(), y.mean()),
                          width=eigenval[0] * j * 2, height=eigenval[1] * j * 2,
                          angle=np.degrees(np.arctan2(*eigenvec[:, 0][::-1])),
                          ec='Black', fill=False, ls='--')

            ax.add_artist(ell)

    return ax


def plot_corr_plot(df: pd.DataFrame, servo: int, ax_joint: plt.Axes,
                   ax_marg_x: plt.Axes, ax_marg_y: plt.Axes) -> plt.Axes:

    logger.info("\t\tservos %i", servo)

    clock_0, clock_1 = np.unique(df['Clock'].values)

    # Split into valid and invalid data
    dfx = df.loc[df['Flag Run Selected'] == False]
    dfv = df.loc[df['Flag Run Selected'] == True]

    # Split valid error signals for the two clocks
    x = dfv.loc[(dfv['Clock'] == clock_0) & (dfv['Servo'] == servo)][
        'Error Signal']
    y = dfv.loc[(dfv['Clock'] == clock_1) & (dfv['Servo'] == servo)][
        'Error Signal']

    # Split invalid error signals for the two clocks
    xx = dfx.loc[(dfx['Clock'] == clock_0) & (dfx['Servo'] == servo)][
        'Error Signal']
    yx = dfx.loc[(dfx['Clock'] == clock_1) & (dfx['Servo'] == servo)][
        'Error Signal']

    # Calculate correlation coefficient and linear fit
    if not x.isnull().values.all() and not y.isnull().values.all():
        corrcoef = np.corrcoef(x, y)[1, 0]
        p, C = np.polyfit(x, y, deg=1, cov=True)
    else:
        corrcoef = np.NaN
        p = np.full(shape=2, fill_value=np.NaN)
        C = np.full(shape=(2, 2), fill_value=np.NaN)
    logger.debug(f"\t\t\tcorrelation coefficient: %.4f", corrcoef)
    logger.debug(f"\t\t\tintercept of linear fit: %.4f +- %.4f", p[1], np.sqrt(
        np.diag(C)[1]))
    logger.debug(f"\t\t\tslope of linear fit: %.4f +- %.4f", p[0], np.sqrt(
        np.diag(C)[0]))

    ax_joint.scatter(xx, yx, s=14, marker='x', c='LightGray')
    ax_joint.scatter(x, y, s=3, c='DimGray')
    ax_joint = plot_ci(x, y, ax=ax_joint, sigmas=3)

    logger.debug("\t\t\t%s", clock_0)
    ax_marg_x = plot_timeseries_hist(x, color='LightGray', ax=ax_marg_x)
    logger.debug("\t\t\t%s", clock_1)
    ax_marg_y = plot_timeseries_hist(y, color='DarkGray', ax=ax_marg_y,
                                     orientation='horizontal')

    ax_joint.set_xlabel(f'{clock_0} Error Signal')
    ax_joint.set_ylabel(f'{clock_1} Error Signal')
    ax_joint.set_xlim(-1, 1)
    ax_joint.set_ylim(-1, 1)

    ax_marg_x.set(xticklabels=[])
    ax_marg_y.set(yticklabels=[])

    ax_joint.text(0.15, 0.90, f"Servo {servo}\nR = {corrcoef*100:.2f}",
                  transform=ax_joint.transAxes,
                  verticalalignment='top')

    return ax_joint, ax_marg_x, ax_marg_y


def plot_fcorr_plot(df: pd.DataFrame, servo: int, ax: plt.Axes) -> plt.Axes:

    logger.info("\t\tservos %i", servo)

    clock_0, clock_1 = np.unique(df['Clock'].values)

    # Split into valid and invalid data
    dfx = df.loc[df['Flag Run Selected'] == False]
    dfv = df.loc[df['Flag Run Selected'] == True]

    # Split valid error signals for the two clocks
    x = dfv.loc[(dfv['Clock'] == clock_0) & (dfv['Servo'] == servo)][
        'Excitation Frequency']
    y = dfv.loc[(dfv['Clock'] == clock_1) & (dfv['Servo'] == servo)][
        'Excitation Frequency']

    x = x.diff()[1:]
    y = y.diff()[1:]

    # Split invalid error signals for the two clocks
    # xx = dfx.loc[(dfx['Clock'] == clock_0) & (dfx['Servo'] == servo)][
    #    'Error Signal']
    # yx = dfx.loc[(dfx['Clock'] == clock_1) & (dfx['Servo'] == servo)][
    #    'Error Signal']

    # Calculate correlation coefficient and linear fit
    if not x.isnull().values.all() and not y.isnull().values.all():
        p, C = np.polyfit(x, y, deg=1, cov=True)
    else:
        p = np.full(shape=2, fill_value=np.NaN)
        C = np.full(shape=(2, 2), fill_value=np.NaN)
    logger.debug(f"\t\t\tintercept of linear fit: %.4f +- %.4f", p[1], np.sqrt(
        np.diag(C)[1]))
    logger.debug(f"\t\t\tslope of linear fit: %.4f +- %.4f", p[0], np.sqrt(
        np.diag(C)[0]))

    # ax_joint.scatter(xx, yx, s=14, marker='x', c='LightGray')
    ax.scatter(x, y, s=3, c='DimGray')
    ax.plot(x, np.polyval(p, x), ls='--', c='Black')

    ax.set_xlabel(f'{clock_0} Frequency Corrections (Hz)')
    ax.set_ylabel(f'{clock_1} Frequency Corrections (Hz)')

    ax.text(0.15, 0.90, f"Servo {servo}\nm = {p[0]:.2f}",
            transform=ax.transAxes, verticalalignment='top')

    return ax
