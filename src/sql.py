# Pull data from BBR logs database;
SELECT_TIMERANGE_BBR_LOGS = 'SELECT unix_timestamp, label, ' \
                            'calibrated_temperature ' \
                            'FROM strontium_lattice.bbr_logs ' \
                            'WHERE (unix_timestamp BETWEEN %s AND %s);'

# Pull data from BBR corrections database;
SELECT_TIMERANGE_SHIFT_ERR_FOR_CLOCK = 'SELECT unix_timestamp, ' \
                                       'channel, shift_err ' \
                                       'FROM ' \
                                       'strontium_lattice.bbr_corrections ' \
                                       'WHERE (unix_timestamp ' \
                                       'BETWEEN %s AND %s) ' \
                                       'AND clock = %s;'
