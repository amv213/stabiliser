import jumbo
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import src.sql as sql
import src.utils as utils
import src.plotting as plotting
from scipy import signal
from pathlib import Path
from functools import reduce
from typing import Tuple, List, Optional
from matplotlib.gridspec import GridSpec

# Spawn module-level logger
logger = logging.getLogger(__name__)


class Clock:
    """Class encapsulating all parameters, data, functions, and statistics
    for optical lattice clock spectroscopy results.

    Example:

        .. code-block:: python
            :emphasize-lines: 3

            import matplotlib.pyplot as plt

            clock = Clock(**args, build_on_creation=True)
            clock.df_raw

            clock.df
            clock.df_stats

            clock.render_all_servos()
            clock.render_stats()

            plt.show()

    Attributes:
        root:               path to the root directory holding data.
        label:              unique identifier label for given clock.
        data_dirs:          list of data directories for given clock.
        transition:         tuple of :math:`^1S_0` to :math:`^3P_0` clock
                            transition stretched states being interrogated.
        spec_time:          spectroscopy time, in seconds.
        spec_type           spectroscopy type.
        contrast:           contrast observed with given interrogation
                            parameters.
        servos:             list of unique servos actuating in experimental
                            sequence
        servo_seq:          ordered list of servo numbers actuating in
                            experimental sequence.
        centre_frequency:   centre servo transition frequency for clock
                            servos, in MHz.
        atom_scaling:       multiplicative factor mapping recorded atom
                            number to actual atom number.
        static_correction:  static fractional frequency correction with
                            respect to reference clock.
        f0:                 nominal Sr87 reference clock transition frequency,
                            in Hz.
        cycle_time:         average servo cycle time in seconds.
        dead_time:          average servo dead time, in seconds.
        run_time:           average clock run time, in seconds. This
                            is the time to obtain data from all servos.
        max_gap_mn:         maximum allowed gap, in minutes, for which
                            invalid data is accepted when assessing dataset
                            validity.
        max_gap:            maximum allowed gap, in number of runs, for which
                            invalid data is accepted when assessing dataset
                            validity.
        df_raw:             dataframe of combined raw servo data, sorted by
                            timestamp, and trimmed to contain an integer
                            number of runs.
        seq_start:          starting timestamp of the collected raw dataframe
        seq_end:            final timestamp of the collected raw dataframe
        df:                 dataframe of processed raw data. Single
                            servo measurements are grouped into chunks,
                            assigned validity flags, and supplemented with
                            BBR corrections.
        df_stats:           dataframe of computed relevant clock statistics
                            for each run.

    Methods:
        pack_data:              scrapes raw servo data files and generates the
                                non-trimmed version of ``df_raw``.
        build_sequence:         pipeline generating ``df``.
        add_bbr:                downloads BBR data from PostgreSQL database and
                                supplements it to data.
        flag_data:              flags each data point and run for validity
                                based on a number of metrics.
        flag_sequence:          parses flagged dataset and flag longest
                                semi-continuous valid interval.
        build_stats:            pipeline generating ``df_stats``.
        get_quad_zeeman_coeff:  calculates quadratic Zeeman coefficient
                                factor for  clock transition being
                                interrogated.
        calc_stats:             calculates aggregate statistics for each
                                data run.
        flag_stats:             flags ``df_stats`` based on ``df`` flags.
        __add__:                synchronises data from two different clocks.
        render_all_servos:      generates dashboards to monitor servo data.
        render_servo:           generates a dashboard to monitor single
                                servo data.
        render_stats:           generates a dashboard to monitor calculated
                                clock statistics.

    References:
        https://www.bipm.org/utils/common/pdf/mep/87Sr_429THz_2018.pdf
    """

    def __init__(self, root: str, label: str, transition: Tuple[float, float],
                 spec_time: float, spec_type: str, contrast: float,
                 servo_seq: List[int], centre_frequency: float,
                 atom_scaling: Optional[float] = 1,
                 static_correction: Optional[float] = 0,
                 max_gap: float = 5,
                 threshold_date_low: str = None,
                 threshold_date_high: str = None,
                 build_on_creation: bool = False) -> None:
        """Creates a ``Clock`` instance.

        Args:
            root:                           path to the root data directory.
                                            This directory should be
                                            storing the individual servo data
                                            directories obtained from LabView
                                            e.g. from different days.
            label:                          unique identifier label for given
                                            clock. Data dirs in ``root``
                                            whose names contain this label
                                            will be parsed for data.
            transition:                     tuple of :math:`^1S_0` to
                                            :math:`^3P_0` clock transition
                                            stretched states being
                                            interrogated.
            spec_time:                      spectroscopy time, in seconds.
            spec_type:                      spectroscopy type. Should be
                                            ``Rabi`` or ``Ramsey``.
            contrast:                       contrast observed with given
                                            interrogation parameters.
            servo_seq:                      ordered list of servo numbers
                                            actuating in experimental sequence.
            centre_frequency:               centre servo transition frequency
                                            for clock servos, in MHz.
            atom_scaling (optional):        multiplicative factor mapping
                                            recorded atom number to actual
                                            atom number. Defaults to ``1``.
            static_correction (optional):   static fractional frequency
                                            correction with respect to
                                            reference clock. Set to ``0`` if
                                            this is the reference clock (
                                            default).
            max_gap (optional):             maximum allowed gap, in number
                                            minutes, for which invalid data
                                            is accepted when assessing dataset
                                            validity.
            threshold_date_low (optional):  timestamp before which to ignore
                                            data. Should be string formatted as
                                            ``YYYY-MM-DD hh:mm:ss``.
                                            Defaults to ``None``, keeping
                                            all available data.
            threshold_date_high (optional): timestamp beyond which to ignore
                                            data. Should be string formatted as
                                            ``YYYY-MM-DD hh:mm:ss``.
                                            Defaults to ``None``, keeping
                                            all available data.
            build_on_creation (optional):   If ``True`` groups servo shots
                                            into experimental runs and
                                            calculates associated single clock
                                            statistics. Defaults to
                                            ``False``, for synchronous clock
                                            sequence statistics.
        """

        self.root = root
        self.label = label

        # Parse root directory for data directories associated with given clock
        self.data_dirs = list(Path(self.root).glob('*' + self.label + '*'))
        self.data_dirs = [fn.resolve() for fn in self.data_dirs]

        self.transition = transition
        self.spec_time = spec_time
        self.spec_type = spec_type
        self.contrast = contrast
        self.servo_seq = servo_seq
        self.centre_frequency = centre_frequency * 1e6  # in Hz
        self.atom_scaling = atom_scaling
        self.static_correction = static_correction

        self.servos = np.unique(servo_seq)

        self.f0 = 429228004229873  # Hz;

        # Set maximum allowed gap of invalid data accepted for analysis
        self.max_gap_mn = max_gap  # in minutes
        self.max_gap = np.NaN  # in number of runs, set by flag_sequence()

        # Set date threshold before/after which to ignore data
        self.threshold_timestamp_low = pd.Timestamp(threshold_date_low).timestamp() \
            if threshold_date_low is not None else threshold_date_low
        self.threshold_timestamp_high = pd.Timestamp(threshold_date_high).timestamp() \
            if threshold_date_high is not None else threshold_date_high

        # Placeholders, set by pack_data()
        self.cycle_time = np.NaN
        self.run_time = np.NaN
        self.dead_time = np.NaN

        # Create raw DataFrame indexed by chronological Shot
        logger.info("\tbuilding raw data")
        self.df_raw = self.pack_data()

        # Trim it to match servo_sequence
        logger.info("\t\ttrimming endpoints to servo sequence")
        self.df_raw = utils.sync_clock(self.df_raw, pattern=self.servo_seq)

        # Servo data start and end times
        self.seq_start = self.df_raw['Time Stamp'].min()
        self.seq_end = self.df_raw['Time Stamp'].max()
        logger.info("\t\t\tstarting on %s", self.seq_start)
        logger.info("\t\t\tending on %s", self.seq_end)

        # TODO: document
        # Extract name of PT-104 channels monitoring temperatures for this clk
        self.bbr_sensors = utils.load_yaml_config().channels[self.label]

        # TODO: document
        # Build BBR dataframe over timerange of interest
        logger.info("\tbuilding bbr data")
        self.df_bbr = self.build_bbr(start=self.seq_start, end=self.seq_end)

        # Flags for whether Plot1, Plot2, and Plot3 have non-NaN values. Set
        # by build_stats()
        self.plot1, self.plot2, self.plot3 = False, False, False

        # Build clock sequence and calculate statistics
        if build_on_creation:
            logger.info("\tbuilding sequence")
            self.df = self.build_sequence(self.df_raw)
            logger.info("\tbuilding statistics")
            self.df_stats = self.build_stats(self.df)
        else:
            logger.info("\tskipped sequence and stats build")

    def __repr__(self) -> str:
        """A debugging-friendly representation of the object.

        Returns:
            representation of the Clock object.
        """

        return f"{self.__class__} ({self.__dict__})"

    def __str__(self) -> str:
        """A human-friendly pretty-print representation of the object.

        Returns:
            annotated representation of the Clock object.
        """

        return ("\nCLOCK PARAMETERS:\n"
                "LABEL:\t{label}\n"
                "DATA DIRS:\t{data_dirs}\n"
                "TRANSITION:\t{transition}\n"
                "SPECTROSCOPY TYPE:\t{spec_type}\n"
                "SPECTROSCOPY TIME:\t{spec_time} s\n"
                "CYCLE TIME:\t{cycle_time:.2f} s\n"
                "DEAD TIME:\t{dead_time:.2f} s\n"
                "CONTRAST:\t{contrast}\n"
                "SERVO SEQUENCE:\t{servo_seq}\n"
                "RUN TIME:\t{run_time:.2f} s\n"
                "CENTRE FREQUENCY:\t{centre_frequency:.2e} Hz\n"
                "ATOM SCALING:\t{atom_scaling}\n"
                "STATIC CORRECTION:\t{static_correction}\n"
                ).format(**self.__dict__)

    def pack_data(self) -> pd.DataFrame:
        """Scrapes servo data directories and packs all data into a
        single dataframe with raw servo data entries sorted chronologically
        and indexed by ``Shot``.

        Servo data should be saved in the standard LabView format we
        generate in the lab. As a minimum:

        - individual servo data for a same experiment should be in subfolders
        called ``ServoX``, where ``X`` is the number of the corresponding servo
        - within these folders, servo data should be at the path
        ``LockDatasets/Lock1.txt``

        Servo data should have at least the following fields:

        - ``Time Stamp`` (from LabView epoch)
        - ``Excitation Frequency``, ``New Center Frequency`` (in MHz)
        - ``First Atom Number Lower (a.u.)``, ``Second Atom Number Upper (
        a.u.)``
        - ``Servo``

        Returns:
            raw servo data frame sorted by timestamp and indexed by ``Shot``.

            index:      ``Shot`` (enumeration of servo entries)
            columns:    ``Time Stamp``, (UNIX epoch)
                        ``Excitation Frequency``, (Hz)
                        ``First Excitation Fraction``,
                        ``Second Excitation Fraction``,
                        ``First Atom Number Lower (a.u.)``,
                        ``Second Atom Number Upper (a.u.)``,
                        ``New Center Frequency``, (Hz)
                        ``Error Signal``,
                        ``Error Signal Int``,
                        ``HL/LH``,
                        ``Unnamed: 10``,
                        ``Lost Lock``,
                        ``Combs AOM Freq``,
                        ``Drift AOM``,
                        ``MWL AOM``,
                        ``1st picture counts 1st Pulse',
                        ``2nd picture counts 1st Pulse',
                        ``3rd picture counts 1st Pulse',
                        ``1st picture counts 2nd Pulse',
                        ``2nd picture counts 2nd Pulse',
                        ``3rd picture counts 2nd Pulse',
                        ``Transfer beat offset frequency',
                        ``Servo``,
                        ``Mean Atom Number (a.u.)``
        """

        logger.info("\t\tscraping data sources")
        servo_data = []
        for servo_n in self.servos:  # for each servo...
            logger.info("\t\t\tservo %i", servo_n)

            dir_data = []
            for data_dir in self.data_dirs:  # for each data directory...

                # Scrape dataset
                servo_fn = data_dir / f'Servo{servo_n}' / 'Lock Datasets/Lock1.txt'
                df = pd.read_csv(servo_fn, sep='\t', index_col=False)

                # Add to other data found for this same servo
                dir_data.append(df)

            # dataframe with all raw data found for this servo
            df = pd.concat(dir_data)
            logger.info("\t\t\t\tglobbed %i datapoints", df.shape[0])

            # PROCESS

            # Convert LabView epoch timestamps to UNIX
            logger.debug("\t\t\t\tconverting timestamps to unix")
            df['Time Stamp'] = utils.labview_timestamp_to_unix(
                timestamp=df['Time Stamp'])
            df = df.sort_values('Time Stamp')
            df.reset_index(drop=True, inplace=True)

            # Filter out data servo data collected before date threshold
            if self.threshold_timestamp_low is not None:
                logger.debug("\t\t\t\tdropping data before requested "
                             "time-range")
                if self.threshold_timestamp_low > df['Time Stamp'].iloc[-1]:
                    raise ValueError("Config parameter `threshold_date_low` "
                                     "is set too high. Decrease it or set it "
                                     "to None.")
                df = df[df['Time Stamp'] > self.threshold_timestamp_low]

            # Filter out data servo data collected after date threshold
            if self.threshold_timestamp_high is not None:
                logger.debug("\t\t\t\tdropping data beyond requested "
                             "time-range")
                if self.threshold_timestamp_high < df['Time Stamp'].iloc[0]:
                    raise ValueError("Config parameter `threshold_date_high` "
                                     "is set too low. Increase it or set it "
                                     "to None.")
                df = df[df['Time Stamp'] < self.threshold_timestamp_high]
            
            # Raw data suffers from timestamping issues so need to deal with it
            logger.info('\t\t\t\tsanitising timestamps')
            df = utils.sanitise_timestamps(df)  # converted to UNIX timestamps

            # add identifier column to track which servo data comes from
            df['Servo'] = servo_n

            # Data for this servo is ready to be packed with others
            servo_data.append(df)

        # Stack data from each servo into a single dataframe

        logger.info("\t\tpacking servo data")
        df = pd.concat(servo_data)

        logger.info("\t\tbuilding raw dataframe")

        # Scale frequencies given in MHz, to Hz
        logger.info("\t\t\tscaling frequencies to Hz")
        df[['Excitation Frequency', 'New Center Frequency']] *= 1e6

        # Scale atom number signal to real atom number
        logger.info("\t\t\tscaling atom number signal to number")
        df[['First Atom Number Lower (a.u.)',
            'Second Atom Number Upper (a.u.)']] *= self.atom_scaling
        logger.info("\t\t\tcalculating mean atom number")
        df['Mean Atom Number (a.u.)'] = df[
            ['First Atom Number Lower (a.u.)',
             'Second Atom Number Upper (a.u.)']].mean(axis=1)

        # Cycle time of the clock is half the mean time-interval between
        # consecutive data entries. Each entry is the result of two clock
        # cycles (high and low sides of the line)
        logger.info("\t\t\tcalculating experimental timings")
        self.cycle_time = df['Time Stamp'].diff().mean() / 2
        self.dead_time = self.cycle_time - self.spec_time

        # The average time of a clock run is given by the average
        # separation between measurements from the same servo
        self.run_time = df.groupby('Servo')['Time Stamp'].diff().agg(
            'mean').mean()

        # Sort all servo data chronologically
        df = df.sort_values('Time Stamp')

        # Re-build index column with unique indexes
        df = df.reset_index(drop=True)

        return df

    def build_bbr(self, start: float, end: float) -> pd.DataFrame:
        """Build a dataframe with relevant BBR data downloaded from
        PostgreSQL database over the time interval of interest.

        Args:
            start:  starting unix timestamp, in seconds.
            end:    ending unix timestamp, in seconds.

        Returns:
            dataframe sorted by timestamp
        """

        # Fetch bbr_logs
        logger.info("\t\tfetching bbr logs")
        df_bbr_logs = self.get_bbr_logs(start=start, end=end)

        # Fetch associated bbr_corrections
        logger.info("\t\tfetching bbr corrections")
        df_bbr_corrections = self.get_bbr_corrections(start=start, end=end)

        # By construction of our PostgreSQL tables, for each bbr_log entry
        # there is an associated bbr_corrections entry with the same timestamp
        # So we can just merge the two tables together
        logger.info("\t\tmerging results")
        df_bbr = pd.merge(df_bbr_logs, df_bbr_corrections, on=["Time Stamp",
                                                               "channel"])

        # Sort and re-build index column just in case
        df_bbr = df_bbr.sort_values('Time Stamp')
        df_bbr.reset_index(drop=True, inplace=True)

        return df_bbr

    def get_bbr_logs(self, start: float, end: float) -> pd.DataFrame:

        # Initialize database connection to download BBR shifts
        logger.info("\t\t\t\tstarting database connection")
        database = jumbo.database.Database()  # <-- path to jumbo.env file
        # should be relative to to top-level script calling this function

        # Get live BBR shifts from database:
        # Open a connection pool.
        with database.open() as pool:
            logger.info("\t\t\t\t\tclient:   \t%s",
                        pool.config.DATABASE_USERNAME)
            logger.info("\t\t\t\t\tserver:   \t%s", pool.config.DATABASE_HOST)
            logger.info("\t\t\t\t\tdatabase: \t%s", pool.config.DATABASE_NAME)
            logger.info("\t\t\t\t\tschema:   \tstrontium_lattice")
            logger.info("\t\t\t\t\ttables:   \t[bbr_logs]")

            # Get an individual connection from the pool.
            with pool.connect(key=1):

                # Pull (unix_timestamp, shift_err) from database;
                results = pool.send(sql.SELECT_TIMERANGE_BBR_LOGS,
                                    subs=(start, end), key=1)

                # Convert results to a pandas DataFrame (if Any)
                if len(results) > 0:
                    logger.info("\t\t\t\tfetched %i entries", len(results))
                    df = jumbo.utils.convert_to_df(results)
                else:
                    logger.warning("\t\t\t\tno bbr_logs data found")
                    df = pd.DataFrame(columns=[
                        'unix_timestamp', 'label', 'calibrated_temperature'])

        # Rename columns for consistency with our other dfs
        df.rename(columns={'unix_timestamp': 'Time Stamp',
                           'label': 'channel'},
                  inplace=True)

        # Keep only temperature readings relative to this clock
        df = df[df['channel'].isin(self.bbr_sensors)]
        # Re-build index column with unique sequential indexes
        df.reset_index(drop=True, inplace=True)

        return df

    def get_bbr_corrections(self, start: float, end: float) -> pd.DataFrame:

        # Initialize database connection to download BBR shifts
        logger.info("\t\t\t\tstarting database connection")
        database = jumbo.database.Database()  # <-- path to jumbo.env file
        # should be relative to to top-level script calling this function

        # Get live BBR shifts from database:
        # Open a connection pool.
        with database.open() as pool:
            logger.info("\t\t\t\t\tclient:   \t%s",
                        pool.config.DATABASE_USERNAME)
            logger.info("\t\t\t\t\tserver:   \t%s", pool.config.DATABASE_HOST)
            logger.info("\t\t\t\t\tdatabase: \t%s", pool.config.DATABASE_NAME)
            logger.info("\t\t\t\t\tschema:   \tstrontium_lattice")
            logger.info("\t\t\t\t\ttables:   \t[bbr_corrections]")

            # Get an individual connection from the pool.
            with pool.connect(key=1):

                # Pull (unix_timestamp, shift_err) from database;
                results = pool.send(sql.SELECT_TIMERANGE_SHIFT_ERR_FOR_CLOCK,
                                    subs=(start, end, self.label), key=1)

                # Convert results to a pandas DataFrame (if Any)
                if len(results) > 0:
                    logger.info("\t\t\t\tfetched %i entries", len(results))
                    df = jumbo.utils.convert_to_df(results)
                else:
                    logger.warning("\t\t\t\tno bbr_corrections data found")
                    df = pd.DataFrame(
                        columns=['unix_timestamp', 'channel', 'shift_err'])

        # Rename timestamps column for consistency with our other dfs
        df.rename(columns={'unix_timestamp': 'Time Stamp'}, inplace=True)

        return df

    def build_sequence(self, df_raw: pd.DataFrame) -> pd.DataFrame:
        """Pipeline parsing raw servo data entries ordered
        chronologically by `Shot` and groups them into chunks of servo
        measurements belonging to the same clock sequence (`Runs`).

        Data is supplemented with BBR shift uncertainty values, and aggregate
        validity flags indicating whether any given run is `Valid` and whether
        it is part of the `Selected` best semi-continuous stretch
        of `Valid` data available.

        Args:
            df_raw: raw dataframe sorted by timestamp and indexed by `Shot`

        Returns:
            dataframe sorted by timestamp, indexed by `Run` and `Shot`,
            and flagged with `Flag Valid Run` and `Flag Run Selected`.
        """

        # Group into run numbers (i.e. complete groups of 4 servo measurements)
        logger.info("\t\t\tgrouping shots")
        df = df_raw.set_index([df_raw.index // len(self.servo_seq),
                               df_raw.index])
        df.index.names = ['Run', 'Shot']

        logger.info("\t\t\tadding bbr data")
        df = self.add_bbr(df)

        logger.info("\t\t\tflagging data")
        df = self.flag_data(df)

        logger.info("\t\t\tselecting longest interval")
        df = self.flag_sequence(df)

        return df

    def add_bbr(self, df: pd.DataFrame) -> pd.DataFrame:
        """Interpolates BBR correction uncertainty values associated with
        each data entry.

        Args:
            df: dataframe sorted by timestamp and indexed by `Run` and `Shot`

        Returns:
            dataframe with added `shift_err` column
        """

        # Initialise empty container in case no bbr data
        if self.df_bbr.empty:
            # FIXME: intialise with NaN instead of 0s?
            df_bbr = pd.DataFrame(0, index=df.index,
                                  columns=['Time Stamp', 'shift_err'])
            df_bbr['Time Stamp'] = df['Time Stamp']

        else:
            df_bbr = self.df_bbr[['Time Stamp', 'shift_err']]

        # TODO: check this merge is really synchronising timestamps as
        #  expected and tweak tolerance parameter (could also do a linear
        #  interpolation)
        old_idx = df.index  # save index through merge collapse
        df = pd.merge_asof(df, df_bbr, on='Time Stamp', direction='nearest')
        df.index = old_idx  # reset index

        return df

    def flag_data(self, df: pd.DataFrame) -> pd.DataFrame:
        """Parses data collected and flags shots where outliers
        have been detected according to a number of custom metrics. Finally, a
        `Flag Shot Valid` is set to `True` if the shot passes under all
        metrics, and the `Flag Run Valid` is set to `True` for each shot in
        the same `Run` if all shots in the same `Run` are valid.

        Args:
            df: dataframe sorted by timestamp and indexed by `Run` and `Shot`

        Returns:
            dataframe flagged for individual metrics, `Flag Shot Valid`,
            and aggregate `Flag Run Valid`
        """

        # We want to check for outliers in the mean atom number of each
        # individual servo - so group by `Servo` before passing to function
        # This function will flag all outliers on a dataset from which all
        # points below a certain threshold are ignored (to avoid skewing
        # median)
        logger.info("\t\t\t\tflagging mean atom number")
        df['Flag Mean Atom Number'] = df.groupby('Servo')[
            'Mean Atom Number (a.u.)'].apply(utils.flag_outliers, 
                                             m=3.5, discard_below=True)
        # ^ increase m if atom number too wobbly (or replace with
        # utils.flag_below_threshold)

        # Flag a buffer number of runs after each run with an atom number drop
        logger.info("\t\t\t\tflagging mean atom number buffer datapoints")
        buffer_size = 3  # 1 to only buffer runs where the dropout happened
        flag_run_had_dropout = df.groupby('Run')['Flag Mean Atom Number'].any()
        buffer_masks = [flag_run_had_dropout.shift(i) for i in range(buffer_size)]
        flag_dropout_buffer = reduce(lambda x1, x2: x1 | x2, buffer_masks)
        df['Flag Dropout Buffer'] = utils.explode(flag_dropout_buffer,
                                                  n=len(self.servo_seq))
        logger.info("\t\t\t\t\tbuffered %i points due to dropouts / %i points",
                    df['Flag Dropout Buffer'].sum(),
                    len(df['Flag Dropout Buffer']))

        # Flag surrogate entries that were added on import of raw servo files
        logger.info("\t\t\t\tflagging surrogate datapoints")
        df['Flag Surrogate'] = df['Excitation Frequency'].isnull()
        logger.info("\t\t\t\t\tflagged %i surrogates / %i points",
                    df['Flag Surrogate'].sum(), len(df['Flag Surrogate']))

        # TODO: Can add flags based on other servo metrics here...
        # df['Flag X'] = ...

        # Combine all flags together. True if any metric raised a flag
        num_flags = len([col for col in df if col.startswith('Flag')])
        any_flag = df.iloc[:, -num_flags:].any(axis=1)

        # True if shot has no flags raised under any metric
        logger.info("\t\t\t\tassessing shot validity")
        df['Flag Shot Valid'] = ~any_flag

        # Create a flag for valid Runs
        logger.info("\t\t\t\tassessing run validity")
        flag_run_valid = df.groupby('Run')['Flag Shot Valid'].all()
        df['Flag Run Valid'] = utils.explode(flag_run_valid,
                                             n=len(self.servo_seq))

        return df

    # TODO: Make this method static and use it in both classes
    def flag_sequence(self, df: pd.DataFrame) -> pd.DataFrame:
        """Parses a dataframe flagged with `Flag Run Valid`, and flags the
        longest sequence of valid runs. Gaps of invalid runs are allowed up
        to the threshold set by `self.max_gap`.

        Args:
            df: dataframe sorted by timestamp and indexed by `Run` and
                `Shot`. Needs `Flag Run Valid` to be a valid key.

        Returns:
            dataframe flagged with `Flag Run Selected` = `True` for all
            `Shots` belonging to a selected `Run`
        """

        # The maximum number of continuous invalid runs allowed in the dataset
        logger.info("\t\t\t\tmaximum gap allowed: %.2f mn", self.max_gap_mn)
        self.max_gap = self.max_gap_mn * 60 // self.run_time

        logger.info("\t\t\t\tparsing data")
        # Get id of valid runs
        valid_run_id = np.unique(
            df[df['Flag Run Valid']].index.get_level_values('Run')
        )

        # Keep only runs which happened close enough in time:
        gap_between_runs = np.diff(valid_run_id)
        idx_boundaries = np.flatnonzero(
            gap_between_runs > self.max_gap)  # boundaries at big gaps
        idx_boundaries = np.r_[0, idx_boundaries, len(
            gap_between_runs)]  # boundaries at edges of dataset
        len_valid_runs = np.diff(
            idx_boundaries)  # size of continuous intervals
        idx_start_of_best_run = idx_boundaries[np.argmax(len_valid_runs)]
        idx_end_of_best_run = idx_boundaries[np.argmax(
            len_valid_runs) + 1]  # Numbers of the runs in longest train
        id_continuous_runs = valid_run_id[
                             idx_start_of_best_run + 1:idx_end_of_best_run + 1]
        logger.info("\t\t\t\tlongest valid interval: %.2f mn",
                    len(id_continuous_runs) * self.run_time / 60)

        #   Note that the above algorithm misses the very first run if longest
        #   continuous run starts at the beginning of the dataset...

        # Create flag for selected Runs
        logger.info("\t\t\t\tflagging valid runs")
        df['Flag Run Selected'] = False
        df.loc[id_continuous_runs, 'Flag Run Selected'] = True

        return df

    def build_stats(self, df: pd.DataFrame) -> pd.DataFrame:
        """Pipeline parsing clock data indexed by `Run` and by `Shot` with
        appropriate aggregate validity flags for each `Run`. Creates a
        separate dataframe with aggregated statistics for each `Run` and
        annotated with corresponding validity flags.

        Args:
            df: clock dataframe sorted by timestamp, indexed by `Run` and
            `Shot`, and flagged with `Flag Valid Run` and `Flag Run Selected`.

        Returns:
            aggregated dataframe with statistics calculated for each `Run`,
            and validity flags for each `Run`. Indexed by `Clock` and by `Run`.
        """

        logger.info("\t\t\tcalculating statistics")
        df_stats = self.calc_stats(df)

        logger.info("\t\t\tflagging statistics")
        df_stats = self.flag_stats(df_stats, df)

        # Add identifier
        df_stats['Clock'] = self.label
        # Index by `Clock` and by `Run`
        df_stats = df_stats.set_index(['Clock', df_stats.index])

        return df_stats

    def get_quad_zeeman_coeff(self):

        transition_to_index = {1/2: 0, 3/2: 1, 5/2: 2, 7/2: 3, 9/2: 4}
        
        zeeman_coeff_matrix = np.array([[54.3, 354.0, np.NaN, np.NaN, np.NaN],
                                        [-136.7, 163.0, 462.7, np.NaN, np.NaN],
                                        [np.NaN, -28.0, 271.7, 571.3, np.NaN],
                                        [np.NaN, np.NaN, 80.7, 380.3, 680.0],
                                        [np.NaN, np.NaN, np.NaN, 189.3, 489.]])

        i = transition_to_index.get(abs(self.transition[0]))
        j = transition_to_index.get(abs(self.transition[1]))

        quad_zeeman_scaling = (zeeman_coeff_matrix[4, 4] / zeeman_coeff_matrix[i, j])**2

        # https://arxiv.org/pdf/1906.06004.pdf
        return (-5.721 * 1e-22) * quad_zeeman_scaling  # Hz^−2,

    def calc_stats(self, df: pd.DataFrame) -> pd.DataFrame:
        """Builds a statistics dataframe by calculating aggregate statistics
        for each `Run` of the clock sequence.

        Args:
            df: clock dataframe sorted by timestamp, indexed by `Run` and
            `Shot`

        Returns:
            aggregated dataframe with statistics calculated for each `Run`.
            Indexed by `Run`.
        """

        df_stats = pd.DataFrame()

        # Put servos in order for easy indexing (i.e. [1 3 2 4] --> [1 2 3 4])
        df = df.sort_values('Servo')

        # Set timestamp for each statistic to the average timestamp of the
        # run that produced it
        logger.info("\t\t\t\tvirtualising timestamps")
        df_stats['Time Stamp'] = df.groupby('Run')['Time Stamp'].mean()

        # Average BBR shift for each run
        logger.info("\t\t\t\tcalculating average BBR uncertainty")
        df_stats['BBR Shift Err'] = df.groupby('Run')['shift_err'].agg(
            'mean')

        logger.info("\t\t\t\tcalculating Plot 1")
        if (1 in self.servos) and (2 in self.servos):

            df_stats['Plot 1 Raw'] = df.groupby('Run')['Excitation Frequency'].agg(
                utils.calc_plot1, centre_freq=self.centre_frequency) / self.f0

            df_stats['Stretched State Splitting A (Hz)'] = df.groupby(
                'Run')['Excitation Frequency'].agg(utils.calc_sss1)

            df_stats['2nd Order Zeeman Shift A'] = df.groupby('Run')[
                'Excitation Frequency'].agg(utils.calc_2o_zeeman1,
                                            coefficient=self.get_quad_zeeman_coeff())

            df_stats['Plot 1'] = df_stats['Plot 1 Raw'] - df_stats[
                '2nd Order Zeeman Shift A'] - df_stats['BBR Shift Err'] + self.static_correction

            self.plot1 = True  # set flag for book-keeping

        else:
            df_stats[['Plot 1 Raw', 'Stretched State Splitting A (Hz)',
                      '2nd Order Zeeman Shift A', 'Plot 1']] = np.NaN

        logger.info("\t\t\t\tcalculating Plot 2")
        if (3 in self.servos) and (4 in self.servos):

            df_stats['Plot 2 Raw'] = df.groupby('Run')['Excitation Frequency'].agg(
                utils.calc_plot2, centre_freq=self.centre_frequency) / self.f0

            df_stats['Stretched State Splitting B (Hz)'] = df.groupby(
                'Run')['Excitation Frequency'].agg(utils.calc_sss2)

            df_stats['2nd Order Zeeman Shift B'] = df.groupby('Run')[
                'Excitation Frequency'].agg(utils.calc_2o_zeeman2,
                                            coefficient=self.get_quad_zeeman_coeff())

            df_stats['Plot 2'] = df_stats['Plot 2 Raw'] - df_stats[
                '2nd Order Zeeman Shift B'] - df_stats['BBR Shift Err'] + self.static_correction

            self.plot2 = True  # set flag for book-keeping

        else:
            df_stats[['Plot 2 Raw', 'Stretched State Splitting B (Hz)',
                      '2nd Order Zeeman Shift B', 'Plot 2']] = np.NaN

        logger.info("\t\t\t\tcalculating Plot 3")
        df_stats['Plot 3 Raw'] = df_stats['Plot 1 Raw'] - df_stats['Plot 2 Raw']
        df_stats['Plot 3'] = df_stats['Plot 1'] - df_stats['Plot 2']
        if self.plot1 and self.plot2:
            self.plot3 = True  # set flag for book-keeping

        logger.info("\t\t\t\tcalculating estimated QPN")
        df_stats['Estimated QPN'] = df.groupby('Run')[
            'Mean Atom Number (a.u.)'].agg(utils.estimate_qpn_instability,
                                           tc=self.cycle_time,
                                           tp=self.spec_time,
                                           c=self.contrast,
                                           spec_type=self.spec_type,
                                           f0=self.f0)

        return df_stats

    @staticmethod
    def flag_stats(df_stats: pd.DataFrame, df: pd.DataFrame) -> pd.DataFrame:
        """Annotate statistics dataframe with valid and selected runs
        information

        Args:
            df_stats: the statistics dataframe. Indexed by `Run`.
            df: the associated clock dataframe sorted by timestamp, indexed by `Run` and
            `Shot`, and flagged with `Flag Valid Run` and `Flag Run Selected`.

        Returns:
            aggregated dataframe with statistics calculated for each `Run`,
            and validity flags for each `Run`. Indexed by `Run`.
        """

        logger.info("\t\t\t\tapplying run validity flags")
        df_stats['Flag Run Valid'] = df.groupby('Run')[
            'Flag Run Valid'].agg('all')

        logger.info("\t\t\t\tapplying run selected flags")
        df_stats['Flag Run Selected'] = df.groupby('Run')[
            'Flag Run Selected'].agg('all')

        return df_stats

    def __add__(self, other: 'Clock') -> 'SyncedClocks':

        return SyncedClocks(self, other)

    def render_all_servos(self, save: bool = True):

        logger.info("\trendering %s servo dashboards", self.label)
        df = self.df.copy()

        axes = []
        for servo in self.servos:
            axs = self.render_servo(df, servo=servo, save=save)
            axes.append(axs)

        return axes

    def render_servo(self, df: pd.DataFrame, servo: int, save: bool = True):

        # TODO: add vertical guide line at midnight of every day

        logger.info("\t\tservo %s dashboard", servo)

        # Select only data relative to this servo
        df = df.loc[self.df['Servo'] == servo]

        fig = plt.figure(figsize=(15, 9))

        fig.suptitle(f"Servo Data Dashboard\n"
                     f"Clock {self.label} - Servo {servo}")

        # Blueprint, setup subplots layout
        gs1 = GridSpec(3, 1)  # num_rows, num_columns

        ax1 = fig.add_subplot(gs1[0, 0])
        ax2 = fig.add_subplot(gs1[1, 0], sharex=ax1)
        ax3 = fig.add_subplot(gs1[2, 0], sharex=ax1)

        ax1 = plotting.plot_timeseries(df, metric='Mean Atom Number (a.u.)',
                                       label='Mean Atom Number', ax=ax1)

        ax2 = plotting.plot_timeseries(df, metric='Excitation Frequency',
                                       label='Excitation Frequency (Hz)',
                                       median=False, ax=ax2)

        ax3 = plotting.plot_timeseries(df, metric='Error Signal',
                                       label='Error Signal', ax=ax3)
        ax3.set_ylim(-1, 1)

        if save:
            utils.save_fig(
                label=f'{self.label.lower()}_servo{servo}_dashboard')

        return ax1, ax2, ax3

    def render_stats(self, save: bool = True):

        logger.info("\trendering %s statistics dashboard", self.label)

        df = self.df_stats.copy()  # make copy to avoid leakage of changes
        
        fig = plt.figure(figsize=(18, 12))

        fig.suptitle(f"Statistics Dashboard\nClock {self.label}")

        # Blueprint, setup subplots layout
        gs1 = GridSpec(3, 2)  # num_rows, num_columns

        ax1 = fig.add_subplot(gs1[0, 0])
        ax2 = fig.add_subplot(gs1[1, 0])
        ax3 = fig.add_subplot(gs1[2, 0])
        ax4 = fig.add_subplot(gs1[0, 1])
        ax5 = fig.add_subplot(gs1[1, 1])
        ax6 = fig.add_subplot(gs1[2, 1])

        logger.info("\t\tPlot 1 Raw")
        ax1 = plotting.plot_timeseries(df, metric='Plot 1 Raw',
                                       label='Plot 1 Raw',
                                       median=False, ax=ax1)

        logger.info("\t\tPlot 2 Raw")
        ax2 = plotting.plot_timeseries(df, metric='Plot 2 Raw',
                                       label='Plot 2 Raw',
                                       median=False, ax=ax2)

        logger.info("\t\tPlot 3 Raw")
        ax3 = plotting.plot_timeseries(df, metric='Plot 3 Raw',
                                       label='Plot 3 Raw', ax=ax3)

        logger.info("\t\t2nd Order Zeeman Shift A")
        ax4 = plotting.plot_timeseries(df, metric='2nd Order Zeeman Shift A',
                                       label='2nd Order Zeeman Shift A',
                                       ax=ax4)

        logger.info("\t\t2nd Order Zeeman Shift B")
        ax5 = plotting.plot_timeseries(df, metric='2nd Order Zeeman Shift B',
                                       label='2nd Order Zeeman Shift B',
                                       ax=ax5)

        logger.info("\t\tBBR Shift Err")
        ax6 = plotting.plot_timeseries(df, metric='BBR Shift Err',
                                       label='BBR Shift Err', ax=ax6)

        if save:
            utils.save_fig(label=f'{self.label.lower()}_statistics_dashboard')

        return ax1, ax2, ax3, ax4, ax5, ax6

    def render_bbr(self, save: bool = True):

        logger.info("\trendering %s bbr dashboard", self.label)

        df_bbr = self.df_bbr.copy()  # make copy to avoid leakage of changes

        num_sensors = len(self.bbr_sensors)
        fig = plt.figure(figsize=(15, 2*(num_sensors+1)))

        fig.suptitle(f"BBR Dashboard\nClock {self.label}")

        # Blueprint, setup subplots layout
        gs1 = GridSpec(num_sensors+1, 1)  # num_rows, num_columns

        # Plot BBR corrections
        ax1 = fig.add_subplot(gs1[0, 0])

        logger.info("\t\tbbr shift uncertainties")
        ax1 = plotting.plot_timeseries(df_bbr, metric='shift_err',
                                       label='BBR Shift Err', ax=ax1,
                                       is_flagged=False)

        # Plot individual sensor temperatures
        logger.info("\t\tbbr temperatures")
        axs = []
        for n, sensor in enumerate(self.bbr_sensors):
            logger.info("\t\t\t%s", sensor)
            # ad a new plot to GridSpec
            ax = fig.add_subplot(gs1[n+1, 0], sharex=ax1)

            df_bbr_sensor = df_bbr.loc[df_bbr['channel'] == sensor]
            
            ax = plotting.plot_timeseries(df_bbr_sensor,
                                           metric='calibrated_temperature',
                                           label=sensor, ax=ax,
                                           is_flagged=False)

            axs.append(ax)

        if save:
            utils.save_fig(label=f'{self.label.lower()}_bbr_dashboard')

        return ax1, axs


class SyncedClocks:

    def __init__(self, clock1: Clock, clock2: Clock):

        # Add identifier to dataframe so that we can keep track when packing
        self.clock1, self.clock2 = clock1, clock2
        self.clock1.df_raw['Clock'], self.clock2.df_raw['Clock'] = clock1.label, clock2.label

        # Create interleaved servo sequence pattern
        seq1, seq2 = clock1.servo_seq, clock2.servo_seq
        if clock1.seq_start > clock2.seq_start:
            seq1, seq2 = seq2, seq1
        self.servo_seq = [val for pair in zip(seq1, seq2) for val in pair]

        self.run_time = 0.5 * (clock1.run_time + clock2.run_time)

        # Set maximum allowed gap to most restrictive maximum allowed gap
        self.max_gap_mn = min(self.clock1.max_gap_mn, self.clock2.max_gap_mn)
        self.max_gap = self.max_gap_mn * 60 // self.run_time

        # Create global raw DataFrame indexed by chronological Shot
        logger.info("\tcollecting all raw data")
        self.df_raw = self.pack_data()
        # Trim it to match servo_seq
        logger.info("\ttrimming endpoints to synchro servo sequence")
        self.df_raw = utils.sync_clock(self.df_raw, pattern=self.servo_seq)

        # Servo data start and end times
        self.seq_start = self.df_raw['Time Stamp'].min()
        self.seq_end = self.df_raw['Time Stamp'].max()
        logger.info("\t\tstarting on %s", self.seq_start)
        logger.info("\t\tending on %s", self.seq_end)

        # Build synchronised and flagged dataset
        logger.info("\tbuilding synchro sequence")
        self.df = self.build_sequence(self.df_raw)
        # Build statistics
        logger.info("\tbuilding synchro statistics")
        self.df_stats = self.build_stats(self.df)

        # Set global flags for which metrics are available to compare
        self.plot1 = self.clock1.plot1 and self.clock2.plot1
        self.plot2 = self.clock1.plot2 and self.clock2.plot2
        self.plot3 = self.plot1 and self.plot2
        
        # Build synchronised clock instability data
        self.df_diff_instability = self.build_diff_instability(self.df_stats)

    def __repr__(self) -> str:
        """A debugging-friendly representation of the object.

        Returns:
            representation of the SyncedClocks object.
        """

        return f"{self.__class__} ({self.__dict__})"

    def __str__(self) -> str:
        """A human-friendly pretty-print representation of the object.

        Returns:
            annotated representation of the SyncedClock object.
        """

        return ("\nSYNCED CLOCKS PARAMETERS:\n"
                "CLOCKS:\t{clock1.label}, {clock2.label}\n"
                "SERVO SEQUENCE:\t{servo_seq}\n"
                "MAX DATA GAP ALLOWED:\t{max_gap_mn} mn\n"
                ).format(**self.__dict__) + "\t" + \
               self.clock1.__str__() + "\t" + \
               self.clock2.__str__() + "\n"

    def pack_data(self) -> pd.DataFrame:
        """Packs the raw trimmed data of the individual clocks into a single
        dataframe sorted chronologically. `Shots` are reindexed to get the
        total raw sequence of servo measurements in the combined clock
        sequence.

        Returns:
            combined raw data, indexed by `Shot`

        """

        # Pack individual raw trimmed clock data and sort all data
        # chronologically
        df_raw = pd.concat([self.clock1.df_raw, self.clock2.df_raw])
        df_raw = df_raw.sort_values('Time Stamp')
        # Re-build index column with unique indexes
        df_raw = df_raw.reset_index(drop=True)

        return df_raw

    def build_sequence(self, df_raw: pd.DataFrame) -> pd.DataFrame:
        """Pipeline parsing 2-clocks raw servo data entries ordered
        chronologically by `Shot` and groups them into chunks of servo
        measurements belonging to the same clock sequence (`Runs`).

        Data is supplemented with BBR shift uncertainty values, and aggregate
        validity flags indicating whether any given run is `Valid` and whether
        it is part of the `Selected` best semi-continuous stretch
        of `Valid` data available.

        Args:
            df_raw: raw dataframe sorted by timestamp and indexed by `Shot`

        Returns:
            dataframe sorted by timestamp, indexed by `Run` and `Shot`,
            and flagged with `Flag Valid Run` and `Flag Run Selected`.
        """

        # Overwrite the single clock raw trimmed data with single clock raw
        # data trimmed at different points where the 2-clocks are synced
        self.clock1.df_raw = df_raw.loc[df_raw['Clock'] == self.clock1.label]
        self.clock2.df_raw = df_raw.loc[df_raw['Clock'] == self.clock2.label]

        assert self.clock1.df_raw.shape == self.clock2.df_raw.shape, \
            "Single clock raw dataframes don't have the same length"

        # Reset indexes so that run numbers are matched
        self.clock1.df_raw = self.clock1.df_raw.reset_index(drop=True)
        self.clock2.df_raw = self.clock2.df_raw.reset_index(drop=True)

        # Build complete dataframe through single-clock pipelines
        logger.info("\t\tbuilding %s sequence", self.clock1.label)
        self.clock1.df = self.clock1.build_sequence(self.clock1.df_raw)
        logger.info("\t\tbuilding %s sequence", self.clock2.label)
        self.clock2.df = self.clock2.build_sequence(self.clock2.df_raw)

        # Assemble dataframes with expected format
        logger.info("\t\tmerging sequences")
        df = pd.concat([self.clock1.df, self.clock2.df])
        df = df.sort_values('Time Stamp')

        # Dataframe is now sorted by timestamp, indexed by `Run` and `Shot`,
        # and flagged with `Flag Valid Run` and `Flag Run Selected` based on
        # the validity of the `Runs` with respect to the single-clock data.

        # Update validity flag based on inter-clock conditions
        logger.info("\t\tflagging synchro data")
        df = self.flag_data(df)

        # Re-select a valid data-run given new validity flags
        logger.info("\t\tselecting longest synchro interval")
        df = self.flag_sequence(df)

        return df

    def flag_data(self, df: pd.DataFrame) -> pd.DataFrame:
        """Updates the `Flag Run Valid` entry for each `Run` to take into
        account validity flags based on the inter-comparison of values
        across the two different clocks.

        Args:
            df: Dataframe sorted by timestamp, indexed by `Run` and `Shot`,
                and flagged with `Flag Valid Run` and `Flag Run Selected`

        Returns:
            dataframe with correct (updated) `Flag Run Valid`
        """

        # Re-asses run validity
        logger.info("\t\t\tmerging single clock flags")
        flag_run_valid = df.groupby('Run')['Flag Run Valid'].agg('all')
        df['Flag Run Valid'] = utils.explode(flag_run_valid, n=len(self.servo_seq))

        # Modify `Flag Run Valid` column here according to metrics which
        # depend on Sr1/Sr2 relationships within single Runs:

        #  Set a flag for each run if difference in error signal between
        #  any two servos is greater than certain value.
        logger.info("\t\t\tflagging error signal difference")
        diff_threshold = 0.2
        logger.debug("\t\t\t\tthreshold for outliers detection set at %.2f",
                     diff_threshold)
        flag_es = df.groupby('Run')['Error Signal'].agg(
            lambda x: (abs(x.diff())[1::2] > diff_threshold).any())
        # Broadcast to dimensions
        exploded_flag_es = utils.explode(flag_es, n=len(self.servo_seq))
        df['Flag Run Valid'] &= ~exploded_flag_es
        logger.info("\t\t\t\tflagged %i outliers / %i points",
                    exploded_flag_es.sum(), len(exploded_flag_es))

        # Add other flags like this here...

        # Broadcast the changes to the individual self.clockX.df for proper
        # bookkeeping. Their individual `Run Validity` will now reflect the
        # influence of the other clock contribution
        logger.debug("\t\t\tupdating cached flags for single clock data")
        self.clock1.df['Flag Run Valid'] = df.groupby(
            'Run')['Flag Run Valid'].apply(lambda x: x[::2].values)
        self.clock2.df['Flag Run Valid'] = df.groupby(
            'Run')['Flag Run Valid'].apply(lambda x: x[::2].values)

        return df

    def flag_sequence(self, df: pd.DataFrame) -> pd.DataFrame:
        """Parses the 2-clocks dataframe and flags the
        longest sequence of valid runs. Gaps of invalid runs are allowed up
        to the threshold set by `self.max_gap`.

        Args:
            df: Dataframe sorted by timestamp, indexed by `Run` and `Shot`,
                and flagged with `Flag Valid Run`

        Returns:
            dataframe with correct (updated) `Flag Run Selected`
        """
        logger.info("\t\t\tmaximum gap allowed: %.2f mn", self.max_gap_mn)

        logger.info("\t\t\tparsing data")
        # Get id of valid runs
        valid_run_id = np.unique(
            df[df['Flag Run Valid']].index.get_level_values('Run')
        )

        # Keep only runs which happened close enough in time:
        gap_between_runs = np.diff(valid_run_id)
        idx_boundaries = np.flatnonzero(gap_between_runs > self.max_gap)  # boundaries at big gaps
        idx_boundaries = np.r_[0, idx_boundaries, len(gap_between_runs)]  # boundaries at edges of dataset
        len_valid_runs = np.diff(idx_boundaries)  # size of continuous intervals
        idx_start_of_best_run = idx_boundaries[np.argmax(len_valid_runs)]
        idx_end_of_best_run = idx_boundaries[np.argmax(len_valid_runs) + 1] # Numbers of the runs in longest train
        id_continuous_runs = valid_run_id[idx_start_of_best_run + 1:idx_end_of_best_run + 1]
        logger.info("\t\t\tlongest valid interval: %.2f mn",
                    len(id_continuous_runs) * self.run_time / 60)
        #   Note that the above algorithm misses the very first run if longest
        #   continuous run starts at the beginning of the dataset...

        # Wipe old flag & update it
        df['Flag Run Selected'] = False
        df.loc[id_continuous_runs, 'Flag Run Selected'] = True

        # Broadcast changes to the individual self.clockX.df for proper
        # bookkeeping. Their Run Selected now reflects the influence of the
        # other clock
        logger.debug("\t\t\tupdating cached flags for single clock data")
        self.clock1.df['Flag Run Selected'] = False
        self.clock1.df.loc[id_continuous_runs, 'Flag Run Selected'] = True
        self.clock2.df['Flag Run Selected'] = False
        self.clock2.df.loc[id_continuous_runs, 'Flag Run Selected'] = True

        return df

    def build_stats(self, df: pd.DataFrame) -> pd.DataFrame:

        # Overwrite the single clock data with synced data
        logger.debug("\t\tupdating cached single clock data")
        self.clock1.df = df.loc[self.df['Clock'] == self.clock1.label]
        self.clock2.df = df.loc[self.df['Clock'] == self.clock2.label]

        # Build complete dataframe through single-clock pipelines
        logger.info("\t\tbuilding %s statistics", self.clock1.label)
        self.clock1.df_stats = self.clock1.build_stats(self.clock1.df)
        logger.info("\t\tbuilding %s statistics", self.clock2.label)
        self.clock2.df_stats = self.clock2.build_stats(self.clock2.df)

        # Assemble dataframes with expected format
        logger.info("\t\tmerging statistics")
        df_stats = pd.concat([self.clock1.df_stats, self.clock2.df_stats])

        return df_stats

    def build_diff_instability(self, df: pd.DataFrame) -> pd.DataFrame:
        # Create dataframe for inter-clock comparison statistics.
        # TODO: finish implementing

        # Split into single-clock dataframes
        df1 = df.loc[self.clock1.label]
        df2 = df.loc[self.clock2.label]

        # Virtualise to a single stats dataframe
        df_diff = utils.virtualize_stats(df1, df2)

        # Calculate difference between metrics of interest
        metrics = ['Plot 1', 'Plot 1 Raw', 'Plot 2', 'Plot 2 Raw',
                   'Plot 3', 'Plot 3 Raw', 'BBR Shift Err']
        for metric in metrics:
            df_diff[f'{metric} Diff'] = df_diff[f'{metric} C2'] - \
                                        df_diff[f'{metric} C1']

        # Add custom metrics which we didn't calculate before
        for plot_n in ['Plot 1', 'Plot 2', 'Plot 3']:
            df_diff[f'{plot_n} No BBR Diff'] = \
                df_diff[f'{plot_n} Diff'] + df_diff['BBR Shift Err Diff']

        return df_diff

    def render_correlations(self, save: bool = True):

        logger.info("\trendering correlations between error signals")

        df = self.df.copy()  # make copy to avoid leakage of changes

        fig = plt.figure(figsize=(12, 9))

        fig.suptitle(f"Correlations between Error Signals")

        # Blueprint, setup subplots layout
        gs1 = GridSpec(9, 9, hspace=0.3, wspace=0.3)  # num_rows,
        # num_columns

        ax1 = fig.add_subplot(gs1[1:4, 0:3])  # box
        ax2 = fig.add_subplot(gs1[0, 0:3])  # hor hist
        ax3 = fig.add_subplot(gs1[1:4, 3])  # vert hist

        ax4 = fig.add_subplot(gs1[1:4, 5:8])  # box
        ax5 = fig.add_subplot(gs1[0, 5:8])  # hor hist
        ax6 = fig.add_subplot(gs1[1:4, 8])  # vert hist

        ax7 = fig.add_subplot(gs1[6:9, 0:3])  # box
        ax8 = fig.add_subplot(gs1[5, 0:3])  # hor hist
        ax9 = fig.add_subplot(gs1[6:9, 3])  # vert hist

        ax10 = fig.add_subplot(gs1[6:9, 5:8])  # box
        ax11 = fig.add_subplot(gs1[5, 5:8])  # hor hist
        ax12 = fig.add_subplot(gs1[6:9, 8])  # vert hist

        ax1, ax2, ax3 = plotting.plot_corr_plot(df, servo=1, ax_joint=ax1,
                                                ax_marg_x=ax2,
                                                ax_marg_y=ax3)

        ax4, ax5, ax6 = plotting.plot_corr_plot(df, servo=2, ax_joint=ax4,
                                                ax_marg_x=ax5,
                                                ax_marg_y=ax6)

        ax7, ax8, ax9 = plotting.plot_corr_plot(df, servo=3, ax_joint=ax7,
                                                ax_marg_x=ax8,
                                                ax_marg_y=ax9)

        ax10, ax11, ax12 = plotting.plot_corr_plot(df, servo=4, ax_joint=ax10,
                                                   ax_marg_x=ax11,
                                                   ax_marg_y=ax12)

        if save:
            utils.save_fig(label='correlations_dashboard')

        return ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11, ax12

    def render_servo_corrections(self, save: bool = True):

        logger.info("\trendering servo frequency corrections")

        df = self.df.copy()  # make copy to avoid leakage of changes

        fig = plt.figure(figsize=(12, 9))

        fig.suptitle(f"Servo frequency corrections dashboard")

        # Blueprint, setup subplots layout
        gs1 = GridSpec(9, 9, hspace=0.3, wspace=0.3)  # num_rows,
        # num_columns

        ax1 = fig.add_subplot(gs1[1:4, 0:3])  # box

        ax2 = fig.add_subplot(gs1[1:4, 5:8])  # box

        ax3 = fig.add_subplot(gs1[6:9, 0:3])  # box

        ax4 = fig.add_subplot(gs1[6:9, 5:8])  # box

        ax1 = plotting.plot_fcorr_plot(df, servo=1, ax=ax1)

        ax2 = plotting.plot_fcorr_plot(df, servo=2, ax=ax2)

        ax3 = plotting.plot_fcorr_plot(df, servo=3, ax=ax3)

        ax4 = plotting.plot_fcorr_plot(df, servo=4, ax=ax4)

        if save:
            utils.save_fig(label='corrections_dashboard')

        return ax1, ax2, ax3, ax4

    def render_plot3_diff(self, save: bool = True):

        logger.info("\trendering differences dashboard")

        # dataframe with inter-clock comparison statistics.
        df_diff = self.df_diff_instability.copy()
        df_diff_v = df_diff.loc[df_diff['Flag Run Selected'] == True]

        # Average interval between valid stats points. This will be
        # slightly different from self.run_time because some of the data in
        # the full dataset is invalid so is skipped
        tau = df_diff_v['Time Stamp'].diff().mean()

        # Calculate average estimated QPN instability across selected dataset:
        qpn_c1 = df_diff_v['Estimated QPN C1'].mean()
        qpn_c2 = df_diff_v['Estimated QPN C2'].mean()
        qpn = np.sqrt(qpn_c1 ** 2 + qpn_c2 ** 2)

        # Pick which metric to plot depending on available servos
        if self.plot3:
            plot_n = 'Plot 3'
        elif self.plot1:
            plot_n = 'Plot 1'
        elif self.plot2:
            plot_n = 'Plot 2'
        else:
            raise ValueError("No available non-NaN Plot #")

        fig = plt.figure(figsize=(12, 12))

        fig.suptitle(f"{plot_n} Differences Dashboard")

        # Blueprint, setup subplots layout
        gs1 = GridSpec(3, 3)  # num_rows, num_columns

        ax1 = fig.add_subplot(gs1[0, 0:2])
        ax2 = fig.add_subplot(gs1[0, 2], sharey=ax1)
        ax3 = fig.add_subplot(gs1[1:3, 0:2])

        logger.info("\t\trendering timeseries")
        ax1 = plotting.plot_timeseries(df_diff, metric=f'{plot_n} Diff',
                                       label=f'{plot_n} Diff', ax=ax1)

        ax2 = plotting.plot_timeseries_hist(df_diff_v[f'{plot_n} Diff'],
                                            color='DarkGray',
                                            orientation='horizontal', ax=ax2)

        logger.info("\t\trendering Allan deviations")
        logger.info("\t\t\taveraging time: %.2f s", tau)  # ~ Run time
        logger.info("\t\t\testimated QPN @ 1s: %.7e", np.sqrt(2)*qpn)

        ax3 = plotting.plot_timeseries_adev(df_diff_v[f'{plot_n} C1'],
                                            tau=tau,
                                            label=f'{plot_n} Sr1',
                                            color='LightGray', ax=ax3)
        ax3 = plotting.plot_timeseries_adev(df_diff_v[f'{plot_n} C2'],
                                            tau=tau,
                                            label=f'{plot_n} Sr2',
                                            color='DarkGray', ax=ax3)
        ax3 = plotting.plot_timeseries_adev(df_diff_v[f'{plot_n} Raw Diff'],
                                            tau=tau,
                                            label='Raw Difference',
                                            color='LightGray',
                                            ax=ax3)
        ax3 = plotting.plot_timeseries_adev(df_diff_v[f'{plot_n} No BBR Diff'],
                                            tau=tau,
                                            label='Difference (No BBR)',
                                            color='DarkGray',
                                            ax=ax3)
        ax3 = plotting.plot_timeseries_adev(df_diff_v[f'{plot_n} Diff'],
                                            tau=tau,
                                            label='Difference', color='Black',
                                            ax=ax3)

        taus = tau * np.arange(1, df_diff_v[f'{plot_n} Diff'].count() + 1)
        ax3.fill_between(taus, np.sqrt(2) * qpn / np.sqrt(taus),
                         color='DarkGray', alpha=0.3)

        ax3.legend()

        if save:
            utils.save_fig(label='differences_dashboard')

        return ax1, ax2, ax3

    def render_instability(self, raw: bool = False, save: bool = True):

        suffix = ' Raw' if raw else ''

        logger.info("\trendering%s interleaved instability dashboard", suffix)

        df = self.df_stats.copy()  # make copy to avoid leakage of changes

        # Split into single-clock dataframes
        df1 = df.loc[self.clock1.label]
        df2 = df.loc[self.clock2.label]

        # Create dataframe for inter-clock comparison statistics
        df_diff1 = utils.build_diff_df(df1, df2, metric=f"Plot 1{suffix}")
        # Create dataframe to interleave to it
        df_diff2 = utils.build_diff_df(df1, df2, metric=f"Plot 2{suffix}")

        # Calculate average estimated QPN instability across selected dataset:
        run_doubly_selected = (df_diff1['Flag Run Selected'] == True) & (
                df_diff2['Flag Run Selected'] == True)
        qpn_c1 = df1.loc[run_doubly_selected]['Estimated QPN'].mean()
        qpn_c2 = df2.loc[run_doubly_selected]['Estimated QPN'].mean()
        qpn = np.sqrt(qpn_c1 ** 2 + qpn_c2 ** 2)

        # Assemble interleaved values (if there are actually enough servos
        # to interleave)
        if df_diff1['Plot Diff'].isnull().all():
            df_diff = df_diff2
            qpn = np.sqrt(2) * qpn
        elif df_diff2['Plot Diff'].isnull().all():
            df_diff = df_diff1
            qpn = np.sqrt(2) * qpn
        else:
            # offset dataframe to virtual interleaved timestamps
            df_diff2['Time Stamp'] += df_diff2['Time Stamp'].diff().mean() / 2
            df_diff = pd.concat([df_diff1, df_diff2]).sort_values('Time Stamp').reset_index(drop=True)

        # FIXME: check this will resist if most of the dataset is outlier
        logger.info("\t\tfiltering out outliers")
        outliers_flag = utils.flag_outliers(df_diff['Plot Diff'], m=3.5)
        df_diff['Flag Run Selected'] = df_diff['Flag Run Selected'].values & \
            (~outliers_flag)

        # Average interval between valid stats points. This will be
        # slightly different from self.run_time because some of the data in
        # the full dataset is invalid so is skipped
        df_diff_v = df_diff.loc[df_diff['Flag Run Selected'] == True]
        tau = df_diff_v['Time Stamp'].diff().mean()

        # Save valid fractional frequency data to file
        utils.save_data(df_diff_v, metric='Plot Diff',
                        fn='interleaved_data.txt')
        y = df_diff_v['Plot Diff']  # alias name for convenience

        fig = plt.figure(figsize=(12, 12))

        fig.suptitle(f"Interleaved{suffix} Statistics Dashboard")

        # Blueprint, setup subplots layout
        gs1 = GridSpec(3, 3)  # num_rows, num_columns

        ax1 = fig.add_subplot(gs1[0, 0:2])
        ax2 = fig.add_subplot(gs1[0, 2], sharey=ax1)
        ax3 = fig.add_subplot(gs1[1:3, 0:2])

        logger.info("\t\trendering timeseries")
        ax1 = plotting.plot_timeseries(df_diff, metric='Plot Diff',
                                       label=f"Sr2-Sr1{suffix}", ax=ax1)

        ax2 = plotting.plot_timeseries_hist(y,
                                            color='DarkGray',
                                            orientation='horizontal', ax=ax2)

        logger.info("\t\trendering Allan deviations")
        logger.info("\t\t\taveraging time: %.2f s", tau)  # ~ Run time / 2
        logger.info("\t\t\testimated QPN @ 1s: %.7e", qpn)

        ax3 = plotting.plot_timeseries_adev(y,
                                            tau=tau,
                                            label=f"Interleaved{suffix} "
                                                  f"Comparison",
                                            color='DarkGray', ax=ax3)

        interleaved_dedrifted = pd.Series(signal.detrend(y))
        ax3 = plotting.plot_timeseries_adev(interleaved_dedrifted, tau=tau,
                                            label='Linear Drift Removed',
                                            color='Black',
                                            ax=ax3)

        taus = tau * np.arange(1, y.count() + 1)
        ax3.fill_between(taus, qpn / np.sqrt(taus),
                        color='DarkGray', alpha=0.3)

        ax3.legend()

        if save:
            tag = suffix.lower().replace(" ", "_")
            utils.save_fig(label=f'interleaved{tag}_dashboard')

        return ax1, ax2, ax3


